package com.aldamm.gasim;

import java.util.List;

public interface DatesToCronConverter {
    String convert(List<String> dates) throws DatesToCronConvertException;

    String getImplementationInfo();
}
