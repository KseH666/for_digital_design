package com.aldamm.gasim;

public class DatesToCronConvertException extends Exception {
    public DatesToCronConvertException(String exception) {
        super(exception);
    }
}
