package com.aldamm.gasim;

import java.util.*;

public class DatesToCronConverterImpl implements DatesToCronConverter {
    @Override
    public String convert(List<String> dates) throws DatesToCronConvertException {
        try {
            List<SimpleDate> simpleDates = new ArrayList<>();
            for (String date : dates) {
                if (date.length() != 19) throw new DatesToCronConvertException("Wrong date");
                if (date.charAt(4) != '-') throw new DatesToCronConvertException("Wrong date");
                if (date.charAt(7) != '-') throw new DatesToCronConvertException("Wrong date");
                if (date.charAt(10) != 'T') throw new DatesToCronConvertException("Wrong date");
                if (date.charAt(13) != ':') throw new DatesToCronConvertException("Wrong date");
                if (date.charAt(16) != ':') throw new DatesToCronConvertException("Wrong date");
                SimpleDate simpleDate = new SimpleDate(
                        Integer.parseInt(date.split("T")[0].split("-")[0]),
                        Integer.parseInt(date.split("T")[0].split("-")[1]),
                        Integer.parseInt(date.split("T")[0].split("-")[2]),
                        Integer.parseInt(date.split("T")[1].split(":")[0]),
                        Integer.parseInt(date.split("T")[1].split(":")[1]),
                        Integer.parseInt(date.split("T")[1].split(":")[2]));
                simpleDates.add(simpleDate);
            }
            int[] dayOfWeak = new int[simpleDates.size()];
            Calendar calendar = new GregorianCalendar();
            for (int i = 0; i < simpleDates.size(); i++) {
                calendar.set(Calendar.YEAR, simpleDates.get(i).year);
                calendar.set(Calendar.MONTH, simpleDates.get(i).month - 1);
                calendar.set(Calendar.DAY_OF_MONTH, simpleDates.get(i).day);
                dayOfWeak[i] = calendar.get(Calendar.DAY_OF_WEEK);
            }
            Arrays.sort(dayOfWeak);
            int[] months = simpleDates.stream().map(SimpleDate::getMonth).distinct().mapToInt(x -> x).sorted().toArray();
            int[] monthDay = simpleDates.stream().map(SimpleDate::getDay).distinct().mapToInt(x -> x).sorted().toArray();
            dayOfWeak = uniqueElements(dayOfWeak);
            int[] hours = simpleDates.stream().map(SimpleDate::getHour).distinct().mapToInt(x -> x).sorted().toArray();
            int[] minutes = simpleDates.stream().map(SimpleDate::getMinute).distinct().mapToInt(x -> x).sorted().toArray();
            int[] secs = simpleDates.stream().map(SimpleDate::getSecond).distinct().mapToInt(x -> x).sorted().toArray();
            return cronSecs(secs) + " " + cronMinutes(minutes, secs) + " "
                    + cronHour(hours, minutes, secs) + " " + cronMonthDay(monthDay, hours, minutes, secs) + " "
                    + cronMonth(months, monthDay, hours, minutes, secs) + " " + cronDay(dayOfWeak, hours, minutes, secs);
        } catch (DatesToCronConvertException e) {
            throw new DatesToCronConvertException("Wrong date");
        }
    }

    private String cronMonth(int[] months, int[] monthDays, int[] hours, int[] mins, int[] secs) {
        if (cronMonthDay(monthDays, hours, mins, secs).equals("*")) return "*";
        if (months.length == 1) return months[0] + "";
        return findDigit(differenceUniqueElements(months), months);
    }

    private String cronMonthDay(int[] monthDays, int[] hours, int[] mins, int[] secs) {
        if (cronHour(hours, mins, secs).equals("*")) return "*";
        if (monthDays.length == 1) return "*";
        int[] uniqueElements = differenceUniqueElements(monthDays);
        if (uniqueElements.length == 1 && uniqueElements[0] == 1) return "*";
        return arrayToString(monthDays);
    }

    private String cronDay(int[] days, int[] hours, int[] mins, int[] secs) {
        if (days.length == 1) return SimpleDate.getDay(days[0]);
        if (cronMonthDay(days, hours, mins, secs).equals("*")) return "*";
        return "?";
    }

    private String cronHour(int[] hours, int[] mins, int[] secs) {
        if (hours.length == 1) return hours[0] + "";
        if (cronMinutes(mins, secs).equals("*")) return "*";
        int[] uniqueElements = differenceUniqueElements(hours);
        if (hours[0] != 0 && uniqueElements.length == 1 && uniqueElements[0] == 1)
            return hours[0] + "-" + hours[hours.length - 1];
        return findDigit(uniqueElements, hours);
    }

    private String cronMinutes(int[] mins, int[] secs) {
        if (mins.length == 1) return mins[0] + "";
        if (cronSecs(secs).equals("*")) return "*";
        int[] uniqueElements = differenceUniqueElements(mins);
        if (uniqueElements.length == 1 && mins.length > 5) return "*";
        if (uniqueElements.length == 2 && uniqueElements[0] == 1 && mins[mins.length - 1] == 59 && mins[0] == 0)
            return "*";
        return findDigit(uniqueElements, mins);
    }

    private String cronSecs(int[] secs) {
        if (secs.length == 1) return secs[0] + "";
        return findDigit(differenceUniqueElements(secs), secs);
    }

    public String findDigit(int[] uniqueElements, int[] array) {
        return switch (uniqueElements[0]) {
            case 1 -> "*";
            case 2 -> "*/2";
            case 3 -> "*/3";
            case 4 -> "*/4";
            case 5 -> "*/5";
            case 6 -> "*/6";
            case 10 -> "*/10";
            case 12 -> "*/12";
            case 15 -> "*/15";
            case 30 -> "*/30";
            default -> arrayToString(array);
        };
    }

    public String arrayToString(int[] array) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < array.length - 1; i++) {
            stringBuilder.append(array[i]).append(",");
        }
        stringBuilder.append(array[array.length - 1]);
        return stringBuilder.toString();
    }

    private int[] uniqueElements(int[] array) {
        int index = 1;
        int[] uniqueElements = new int[1];
        boolean exist;
        uniqueElements[0] = array[0];
        for (int value : array) {
            exist = false;
            for (int i : uniqueElements) {
                if (i == value) {
                    exist = true;
                    break;
                }
            }
            if (!exist) {
                index++;
                int[] interim = new int[index];
                System.arraycopy(uniqueElements, 0, interim, 0, uniqueElements.length);
                interim[index - 1] = value;
                uniqueElements = new int[index];
                System.arraycopy(interim, 0, uniqueElements, 0, interim.length);
            }
        }
        return uniqueElements;
    }

    public int[] differenceUniqueElements(int[] array) {
        int[] difference = new int[array.length - 1];
        for (int i = 0; i < array.length - 1; i++) {
            difference[i] = array[i + 1] - array[i];
        }
        return uniqueElements(difference);
    }

    @Override
    public String getImplementationInfo() {
        return "Author: Al-Damm Gasim Muhammadovich\n" + "Class: " + this.getClass() + "\nPackage: " + this.getClass().getPackageName() + "\nGitLab: https://gitlab.com/KseH666/for_digital_design";
    }
}
